#!/bin/bash

## Phineas' backup utility v0.1
#
## Script goals and description
#    This script is a simple backup script. You can specify the source and destination folders, in CLI or graphics with zenity.
#    You can also use it in auto mode. With this mode, the script use as destination and source the folders or files specified in the config file located
#    at ~/.config/backupUtility.conf.
#
## Dependancies
#    - zenity
#
## License 
#    (CC) BY-NC-SA by Phineas for all the code

function help {
    echo "-----------------------------";
    echo "Phineas' backup utility v0.1";
    echo "-----------------------------";
    echo "Options :";
    echo -e "-g\t(graphical)display windows to select folders (use zenity)"
    echo -e "-bckd\tspecify backup folder"
    echo -e "-ftb\tspecify folder to backup"
    echo -e "-num\tspecify number of backups to keep"
    echo -e "-h\tdisplay this help and exit"
    echo "-----------------------------";
    echo "Config file : ~/.config/backupUtility.conf"
    echo "-----------------------------";
    echo "License : (CC) BY-NC-SA by Phineas"

    exit 0;
}

# config file location
conf=~/.config/backupUtility.conf;

# about config file
if [ ! -f $conf ]
then
    echo -e "# Backup utility config file\nbackupDir=\nfileToBack=\nbackupNum=" > $conf;
    echo -e '\033[1;31mThe config file was created but he need to be edited : ~/.config/backupUtility.conf \033[0m';
fi

# default options (loaded from config file)
if [ -f $conf ]
then
    backupDir=$HOME$(cat $conf | grep "backupDir" | cut -d "=" -f 2);
    fileToBack=$HOME$(cat $conf | grep "fileToBack" | cut -d "=" -f 2);
    backupNum=$(cat $conf | grep "backupNum" | cut -d "=" -f 2);
fi

# this script doesnt work without arguments, for security
if [ $# -eq 0 ]
then
    help;
fi

# arguments parsing
for arg in "$@"
do
    if [ "$arg" = "-h" ]
    then
	help;
    elif [ "$arg" = "-g" ]
    then
	zenity --info --text="Choose destination folder";
	backupDir=$(zenity --file-selection --directory);
	zenity --info --text="Choose folder who need backup";
	fileToBack=$(zenity --file-selection --directory);
    elif [ "$arg" = "-auto" ]
    then
	backupDir=$(cat $conf | grep "backupDir" | cut -d "=" -f 2);
	fileToBack=$(cat $conf | grep "fileToBack" | cut -d "=" -f 2);
	backupNum=$(cat $conf | grep "backupNum" | cut -d "=" -f 2);	
    elif [ $(echo "$arg" | cut -d "=" -f 1) = "-bckd" ]
    then
	backupDir=$(echo "$arg" | cut -d "=" -f 2);
	if [ "${backupDir:0:1}" = "~" ]
	then
	    backupDir="$HOME${backupDir:1:${#backupDir}}";
	elif [ "${backupDir:0:1}" = "/" ]
	then
	    backupDir="$backupDir";
	else
	    backupDir=$(pwd)/$(echo "$arg" | cut -d "=" -f 2);
	fi
	echo -e "Destination : \t$backupDir";
	
    elif [ $(echo "$arg" | cut -d "=" -f 1) = "-ftb" ]
    then
	fileToBack=$(echo "$arg" | cut -d "=" -f 2);
	if [ "${fileToBack:0:1}" = "~" ]
	then
	    fileToBack=$HOME${fileToBack:1:${#fileToBack}};
	elif [ "${fileToBack:0:1}" = "/" ]
	then
	    fileToBack=$fileToBack;
	else
	    fileToBack=$(pwd)/$(echo "$arg" | cut -d "=" -f 2);
	fi
	echo -e "Source : \t$fileToBack";
    elif [ $(echo "$arg" | cut -d "=" -f 1) = "-num" ]
    then
	backupNum=$(echo "$arg" |  cut -d "=" -f 2);
    fi
done

# check if backup folder exist
if [ ! -d "$backupDir" ]
then
    mkdir "$backupDir";
fi

# count the number of backups
backupNumPre=$(ls "$backupDir" | wc -l);

# delete older backups if number is greater than the number wanted
if [ $backupNumPre -gt $backupNum ]
then
    echo "Suppressing old backups...";
    for i in $(seq 1 $(($backupNumPre - $backupNum)))
    do
	echo "$backupDir/$(ls -t $backupDir | tail -1) : deleted";
	rm -rf "$backupDir/$(ls -t $backupDir | tail -1)";
    done
    echo "done";
fi

# create new backup
today=$(date '+%D %T' | tr -s " " "-" | tr -s "/" "-")
echo -e "Backuping \e[1m$fileToBack\e[21m to \e[1m$backupDir\e[21m with \e[1m$today"-"$(echo $fileToBack | rev | cut -d "/" -f 1 | rev)\e[21m as name"
cp -r $fileToBack $backupDir/$today"-"$(echo $fileToBack | rev | cut -d "/" -f 1 | rev);
